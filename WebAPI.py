from locust import HttpLocust, TaskSet, task, between
import logging, sys
import json
from credentials import *
from caregivercredentials import *


class LoginWithUniqueUsersSteps(TaskSet):
    username = "NOT_FOUND"
    password = "NOT_FOUND"
    caregiverusername = "NOT_FOUND"
    caregiverpassword = "NOT_FOUND"


    responsedict = {}
    responsedict1 = {}
    responsedict2 = {}

    def on_start(self):
            if len(USER_CREDENTIALS) > 0:
                self.username, self.password = USER_CREDENTIALS.pop()
            if len(CAREGIVER_CREDENTIALS) > 0:
                self.caregiverusername, self.caregiverpassword = CAREGIVER_CREDENTIALS.pop()

   
    @task(1)
    def addprescription(self):
        response = self.client.post("/dayamed-web/api/login?lang=en",
                                    json={"languagePreference": "en", "password": self.password, "typeofdevice": "web",
                                          "username": self.username}, verify=False, catch_response=True)
        str_Login_response = json.dumps(response.json())
        self.responsedict = json.loads(str_Login_response)
        print(self.responsedict['jwtToken'])
        print(self.responsedict['userDetails']['id'])
        self.client.post("/dayamed-web/api/rest/w/patients?lang=en",  json={"limit":10,"providerId":[],"offset":0,"sortBy":"lastName","sortOrder":"asc","query":""}, headers={"authorization":self.responsedict['jwtToken']})
        self.client.post("/dayamed-web/api/rest/w/patients?lang=en",  json={"limit":10,"providerId":[],"offset":1,"sortBy":"lastName","sortOrder":"asc","query":""}, headers={"authorization":self.responsedict['jwtToken']})
        self.client.post("/dayamed-web/api/rest/w/patients?lang=en",  json={"limit":10,"providerId":[],"offset":2,"sortBy":"lastName","sortOrder":"asc","query":""}, headers={"authorization":self.responsedict['jwtToken']})
        response1 = self.client.post("/dayamed-web/api/rest/w/patients?lang=en",  json={"limit":10,"providerId":[],"offset":0,"sortBy":"lastName","sortOrder":"asc","query":""}, headers={"authorization":self.responsedict['jwtToken']})
        str1 = json.dumps(response1.json())
       # print("Line46 "+str1)
        self.responsedict1 = json.loads(str1)
        print(self.responsedict1['patients'][0]['id'])
        self.client.post("/dayamed-web/api/rest/patients/"+str(self.responsedict1['patients'][0]['id'])+"/prescriptions/add?lang=en", headers={"authorization":self.responsedict['jwtToken']}, json ={"canPatientModify": "true","comment": "","commodityInfoList": [],"deviceInfoList": [],"diagnosisList": [{"id": "35", "name": "test3"}],"0": {"id": "35", "name": "test3"},"diseaseInfoList": [{"id": "null", "disease": {"id": "5", "name": "null", "type": "null", "description": "null", "category": "null"}}],"0": {"id": "null", "disease": {"id": "5", "name": "null", "type": "null", "description": "null", "category": "null"}}, "dosageDevices": [],"dosageInfoList": [{"id": "null"}],"0": {"id": "null"},"expectedAdherence": "null","id": "null","patientId": "null","solution": "dcs"}, verify=False)
        self.client.get("/dayamed-web/api/rest/providers/byuser/"+str(self.responsedict['userDetails']['id'])+"?lang=en", headers={"authorization": self.responsedict['jwtToken']})
        self.client.get("/dayamed-web/api/rest/patients/"+str(self.responsedict1['patients'][0]['id'])+"?lang=en", headers={"authorization":self.responsedict['jwtToken']})
        self.client.get("/dayamed-web/api/rest/patients/"+str(self.responsedict1['patients'][1]['id'])+"?lang=en", headers={"authorization":self.responsedict['jwtToken']})
        response2 = self.client.get("/dayamed-web/api/rest/patients/"+str(self.responsedict1['patients'][0]['id'])+"?lang=en", headers={"authorization":self.responsedict['jwtToken']})
        str2 = json.dumps(response2.json())
        #print("Line 53 "+str2)
        response3 = self.client.get("/dayamed-web/api/rest/patients/"+str(self.responsedict1['patients'][1]['id'])+"?lang=en", headers={"authorization":self.responsedict['jwtToken']})
        str3 = json.dumps(response2.json())
        #print("Line 54 "+str2)
        self.client.get("/dayamed-web/api/rest/adherence/patient/analytics/"+str(self.responsedict1['patients'][0]['id'])+"?lang=en", headers={"authorization":self.responsedict['jwtToken']})
        self.client.post("/dayamed-web/api/rest/utility/patient/calendernotifications/byuserid/"+str(self.responsedict1['patients'][0]['userDetails']['id'])+"?lang=en", json={"start":"2021-10-31","end":"2021-12-05"}, headers={"authorization":self.responsedict['jwtToken']})
        self.client.post("/dayamed-web/api/rest/utility/patient/calendernotifications/byuserid/"+str(self.responsedict1['patients'][1]['userDetails']['id'])+"?lang=en", json={"start":"2021-10-31","end":"2021-12-05"}, headers={"authorization":self.responsedict['jwtToken']})
        response4 = self.client.post("/dayamed-web/api/rest/utility/patient/calendernotifications/byuserid/"+str(self.responsedict1['patients'][0]['userDetails']['id'])+"?lang=en",  json={"start":"2021-10-31","end":"2021-12-05"}, headers={"authorization":self.responsedict['jwtToken']})
        str4 = json.dumps(response4.json())
        #print("Line 63 "+str4)
        self.client.get("/dayamed-web/api/rest/w/pharmacist/patients?limit=10&offset=0&sortBy=lastActiveTime&sortOrder=desc&query=and&lang=en", headers={"authorization":self.responsedict['jwtToken']})
        response6 = self.client.get("/dayamed-web/api/rest/w/pharmacist/patients?limit=10&offset=0&sortBy=lastActiveTime&sortOrder=desc&query=and&lang=en", headers={"authorization":self.responsedict['jwtToken']})
        str6 = json.dumps(response6.json())
        print("Line 68 "+str6)
        self.client.get("/dayamed-web/api/rest/patients/"+str(self.responsedict1['patients'][0]['id'])+"/prescriptions?prescriptionAdherenceRequired=true&lang=en", headers={"authorization":self.responsedict['jwtToken']})
        response5 = self.client.get("/dayamed-web/api/rest/patients/"+str(self.responsedict1['patients'][0]['id'])+"/prescriptions?prescriptionAdherenceRequired=true&lang=en", headers={"authorization":self.responsedict['jwtToken']})
        str5 = json.dumps(response5.json())
        #print("Line 71 "+str5)
        self.client.get("/dayamed-web/api/rest/adherence/intervaledreport?pharmacyId=1&lang=en", headers={"authorization":self.responsedict['jwtToken']})

class LoginWithUniqueUsersTest(HttpLocust):
    task_set = LoginWithUniqueUsersSteps
    wait_time = between(10, 20)
